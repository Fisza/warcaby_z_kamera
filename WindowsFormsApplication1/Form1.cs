﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {


        private Capture _capture = null;
        public bool captureInProgress;
        public bool saveToFile;
        Size patternSize = new Size(5, 5);
        List<VectorOfPoint> corners = new List<VectorOfPoint>();
        public Form1()
        {
            InitializeComponent();
            CvInvoke.UseOpenCL = false;
            try
            {
                _capture = new Capture();
                _capture.ImageGrabbed += ProcessFrame;
                _capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }
        private void ProcessFrame(object sender, EventArgs arg)
        {

            Mat frame = new Mat();
            _capture.Retrieve(frame, 0);
            captureImageBox.Image = frame;
            if (saveToFile)
            {
                frame.Save(@"C:\testy\MyPic.jpg");
                Mat uimage = new Mat();
                CvInvoke.CvtColor(frame, uimage, ColorConversion.Bgr2Gray);
                imageBox1.Image = uimage;

                saveToFile = !saveToFile;
            }
            Image<Gray, Byte> imageimput = new Image<Gray, Byte>(@"C:\testy\MyPic.jpg");
            var cornerPoints = new VectorOfPoint();
            corners.Add(cornerPoints);
            bool result = CvInvoke.FindChessboardCorners(imageimput, patternSize, cornerPoints, CalibCbType.AdaptiveThresh | CalibCbType.FastCheck | CalibCbType.NormalizeImage);
            CvInvoke.DrawChessboardCorners(imageimput, patternSize, cornerPoints, result);
            if (!result) Console.WriteLine("niema");
            foreach (Point cornerPoint in cornerPoints.ToArray())
            {
                Console.WriteLine(cornerPoint.X + ", " + cornerPoint.Y);
            }
            imageBox1.Image = imageimput;


        }

        private void Pstryk_Click(object sender, EventArgs e)
        {
            saveToFile = !saveToFile;
        }
    }
}