﻿namespace Camera_chequers
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.PlayButton = new System.Windows.Forms.Button();
            this.HowToPlayButton = new System.Windows.Forms.Button();
            this.AuthorsButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // PlayButton
            // 
            this.PlayButton.Location = new System.Drawing.Point(71, 22);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(97, 50);
            this.PlayButton.TabIndex = 0;
            this.PlayButton.Text = "Zagraj";
            this.toolTip1.SetToolTip(this.PlayButton, "Kliknij, aby wyświetlic planszę do gry.");
            this.PlayButton.UseVisualStyleBackColor = true;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // HowToPlayButton
            // 
            this.HowToPlayButton.Location = new System.Drawing.Point(71, 96);
            this.HowToPlayButton.Name = "HowToPlayButton";
            this.HowToPlayButton.Size = new System.Drawing.Size(97, 50);
            this.HowToPlayButton.TabIndex = 1;
            this.HowToPlayButton.Text = "Jak grać?";
            this.toolTip1.SetToolTip(this.HowToPlayButton, "Kliknij, aby wyświetlić zasady gry.");
            this.HowToPlayButton.UseVisualStyleBackColor = true;
            this.HowToPlayButton.Click += new System.EventHandler(this.HowToPlayButton_Click);
            // 
            // AuthorsButton
            // 
            this.AuthorsButton.Location = new System.Drawing.Point(71, 170);
            this.AuthorsButton.Name = "AuthorsButton";
            this.AuthorsButton.Size = new System.Drawing.Size(97, 50);
            this.AuthorsButton.TabIndex = 2;
            this.AuthorsButton.Text = "Autorzy";
            this.toolTip1.SetToolTip(this.AuthorsButton, "Kliknij, aby wyświetlić informacje o autorach.");
            this.AuthorsButton.UseVisualStyleBackColor = true;
            this.AuthorsButton.Click += new System.EventHandler(this.AuthorsButton_Click);
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(71, 244);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(97, 50);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.Text = "Wyjście";
            this.toolTip1.SetToolTip(this.ExitButton, "Kliknij, aby zamknąć aplikację.");
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Camera_chequers.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(239, 307);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.AuthorsButton);
            this.Controls.Add(this.HowToPlayButton);
            this.Controls.Add(this.PlayButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Warcaby";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button PlayButton;
        private System.Windows.Forms.Button HowToPlayButton;
        private System.Windows.Forms.Button AuthorsButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}