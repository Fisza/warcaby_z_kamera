﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI;


namespace Game_logic
{
    public class Rule
    {
        public Board board;
        public Player whoturn;
        public bool kill;
        public bool valid;
        public bool removed;
        public int removedX;
        public int removedY;
        public bool transformed;
        public Scheme canPawnKill;
        delegate void SetLogCall(string tekst);
        public  BoardForm board_tmp;


        public Rule(BoardForm board_form)
        {
            this.board_tmp = board_form;
            board = new Board();
            Start();
            kill = false;
        }
        private  void SetLog(string txt)
        {
            
           if (board_tmp.LogTextBox.InvokeRequired)
            {
                SetLogCall f = new SetLogCall(SetLog);
                board_tmp.Invoke(f, new object[] { txt });
            }
            else
            {
                board_tmp.LogTextBox.AppendText("#" + txt + "\n");
            }
        }
        public void Start()
        {
            board.create_board();
            whoturn = Player.PlayerOne;
        }
        //zmiana kolejności
        public void ChangeTurn()
        {
            if (whoturn == Player.PlayerOne)
                whoturn = Player.PlayerTwo;
            else whoturn = Player.PlayerOne;
        }

        //sprawdza kto wygrał
        public void CheckWin()
        {
            if (board.PawnPlayerOne == 0)
            {
                Console.WriteLine("Gracz Drugi wygrał");
                SetLog("Gracz Drugi wygrał");
                MessageBox.Show("Gracz Drugi wygrał", "Zwycięstwo");

            }
            else if (board.PawnPlayerTwo == 0)
            {
                Console.WriteLine("Gracz Pierwszy Wygrał");
                SetLog("Gracz Pierwszy wygrał");
                  MessageBox.Show("Gracz Pierwszy wygrał", "Zwycięstwo");
            }
        }
        public void Turn(int startX, int startY, int endX, int endY)
        {
            Scheme szachownica = board[startY, startX];
            valid = false;
            removed = false;
            transformed = false;
            if (szachownica.pawn == PawnType.Pawn)
            {
                TurnNormalPawn(szachownica, startX, startY, endX, endY);
            }
            else if (szachownica.pawn == PawnType.PawnQueen)
            {
                TurnQueenPawnTest(szachownica, startX, startY, endX, endY);
            }
        }
        public void TurnMove(int startX, int startY, int endX, int endY)
        {

            if (kill)
            {
                board.Remove(canPawnKill);
                removedX = canPawnKill.X;
                removedY = canPawnKill.Y;
                MessageBox.Show("Za niebicie tracisz zycie!");
                kill = false;
                removed = true;
            }
            else
            {
                Scheme szachownica = board[startY, startX];
                transformed = TransformationPawn(szachownica, endY);
                valid = board.try_move(startX, startY, endX, endY);


                Board_manager board_mamnger_tmp = new Board_manager();
                int[] moveFromTmp = new int[] { startX, startY };
                int[] moveToTmp = new int[] { endX, endY };
                string[] moveFromTmp2 = board_mamnger_tmp.ReturnLetterPosition(moveFromTmp);
                string[] moveToTmp2 = board_mamnger_tmp.ReturnLetterPosition(moveToTmp);
                SetLog("Ruch z: " + string.Join("\n", moveFromTmp2) + " do: " + string.Join("\n", moveToTmp2));
            }
            ChangeTurn();

            CheckWin();
        }

        public void MovePawn(Scheme board, int endX, int endY)
        {

        }
        public void Kill(Scheme szachownica, int endX, int endY, int centerX, int centerY)
        {
            transformed = TransformationPawn(szachownica, endY);
            board.move(szachownica, endX, endY);
            board.Remove(centerX, centerY);
            removedX = centerX;
            removedY = centerY;
            removed = true;
            kill = false;

            if (!PawnCanKill(szachownica, endX, endY))

                ChangeTurn();

            CheckWin();
        }
        //sprawdza czy na szachownicy są jakieś możliwości bicia dla danego gracza
        public void CheckAllKill()
        {
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if ((y + x) % 2 != 0 && board[y, x] != null && board[y, x].Player == whoturn) // porównuje gracza z tym którego jest ruch
                    {
                        if (PawnCanKill(board[y, x], x, y))
                            return;
                    }
                }
            }
        }
        
        //sprawdza, czy pion może bić
        public bool PawnCanKill(Scheme draftboard, int startX, int startY)
        {
            if ((startY - 1 >= 0 && startX + 1 <= 7) &&
                (startY - 2 >= 0 && startX + 2 <= 7) &&
                board[startY - 1, startX + 1] != null &&
                board[startY - 1, startX + 1].Player != draftboard.Player &&
                board[startY - 2, startX + 2] == null)
            {
                kill = true;
                canPawnKill = draftboard;
            }
            else if ((startY - 1 >= 0 && startX - 1 >= 0) &&
                (startY - 2 >= 0 && startX - 2 >= 0) &&
                board[startY - 1, startX - 1] != null &&
                board[startY - 1, startX - 1].Player != draftboard.Player &&
                board[startY - 2, startX - 2] == null)
            {
                kill = true;
                canPawnKill = draftboard;
            }
            else if ((startY + 1 <= 7 && startX - 1 >= 0) &&
                (startY + 2 <= 7 && startX - 2 >= 0) &&
                board[startY + 1, startX - 1] != null &&
                board[startY + 1, startX - 1].Player != draftboard.Player &&
                board[startY + 2, startX - 2] == null)
            {
                kill = true;
                canPawnKill = draftboard;
            }
            else if ((startY + 1 <= 7 && startX + 1 <= 7) &&
                (startY + 2 <= 7 && startX + 2 <= 7) &&
                board[startY + 1, startX + 1] != null &&
                board[startY + 1, startX + 1].Player != draftboard.Player &&
                board[startY + 2, startX + 2] == null)
            {
                kill = true;
                canPawnKill = draftboard;
            }
            //MessageBox.Show("Usun pionek", "Bicie");
            return kill;

        }


        public void TurnNormalPawn(Scheme szachownica, int startX, int startY, int endX, int endY)
        {
            CheckAllKill();
            if ((board[endY, endX] == null && szachownica.Player == Player.PlayerOne && ((szachownica.X - 1 == endX && szachownica.Y + 1 == endY) || (szachownica.X + 1 == endX && szachownica.Y + 1 == endY))) ||
            (board[endY, endX] == null && szachownica.Player == Player.PlayerTwo && ((szachownica.X - 1 == endX && szachownica.Y - 1 == endY) || (szachownica.X + 1 == endX && szachownica.Y - 1 == endY))))
            {
                TurnMove(startX, startY, endX, endY);
            }
            else
            {
                int centerX = 0;
                int centerY = 0;

                if (szachownica.X + 2 == endX && szachownica.Y - 2 == endY)
                {
                    centerX = szachownica.X + 1;
                    centerY = szachownica.Y - 1;
                }
                else if (szachownica.X - 2 == endX && szachownica.Y - 2 == endY)
                {
                    centerX = szachownica.X - 1;
                    centerY = szachownica.Y - 1;
                }
                else if (szachownica.X - 2 == endX && szachownica.Y + 2 == endY)
                {
                    centerX = szachownica.X - 1;
                    centerY = szachownica.Y + 1;
                }
                else if (szachownica.X + 2 == endX && szachownica.Y + 2 == endY)
                {
                    centerX = szachownica.X + 1;
                    centerY = szachownica.Y + 1;
                }
                

                if (board[centerY, centerX] != null && board[centerY, centerX].Player != szachownica.Player)
                {
                    TurnKill(szachownica, endX, endY, centerX, centerY);
                    //MessageBox.Show("Za niebicie tracisz życie!", "Usuń swojego pionka");


                    Board_manager board_mamnger_tmp = new Board_manager();
                    
                    int[] removedTmp = new int[] { endX, endY };
                    string[] removedTmp2 = board_mamnger_tmp.ReturnLetterPosition(removedTmp);
                    SetLog("Usuniętio piona z: " + string.Join("\n", removedTmp) );
                }
                else
                {
                    MessageBox.Show("Ruch niepoprawny!", "Źle");
                    SetLog("Błędny ruch");
                }
            }
        }
        //to poniżej do poprawki
        void TurnQueenPawnTest(Scheme szachownica, int startX, int startY, int endX, int endY)
        {
            int centerX = 0;
            int centerY = 0;
            int pawn = 0;
            int x = startX;
            int y = startY;
            
            if (board[endY, endX] == null && ((y + x) % 2) != 0 && board[startY, startX].player == Player.PlayerOne)
            {
                if (startX > endX && startY > endY)
                {
                    while (x != endX && y != endY)
                    {
                        x--;
                        y--;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            pawn = 8;
                            break;
                        }

                        if (pawn > 1)
                        {
                          //  MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                          
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                      //  MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX > endX && startY < endY)
                {
                    while (x != endX && y != endY)
                    {
                        x--;
                        y++;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                          //  MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }

                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                      //  MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX < endX && startY > endY)
                {
                    while (x != endX && y != endY)
                    {
                        x++;
                        y--;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                           // MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                      //  MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX < endX && startY < endY)
                {
                    while (x != endX && y != endY)
                    {
                        x++;
                        y++;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                          //  MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                       // MessageBox.Show("Probujesz zbic swojego piona!");

                    }

                }
                ///int startX, int startY, int endX, int endY
               
            }
            if (board[endY, endX] == null && ((y + x) % 2) != 0 && board[startY, startX].player == Player.PlayerTwo)
            {
                if (startX > endX && startY > endY)
                {
                    while (x != endX && y != endY)
                    {
                        x--;
                        y--;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                           // MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                      //  MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX > endX && startY < endY)
                {
                    while (x != endX && y != endY)
                    {
                        x--;
                        y++;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                           // MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                       // MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX < endX && startY > endY)
                {
                    while (x != endX && y != endY)
                    {
                        x++;
                        y--;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                            //MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                       // MessageBox.Show("Probujesz zbic swojego piona!");

                    }
                }
                else if (startX < endX && startY < endY)
                {
                    while (x != endX && y != endY)
                    {
                        x++;
                        y++;
                        if (board[y, x] != null && board[y, x].player == Player.PlayerOne)
                        {
                            centerX = x;
                            centerY = y;
                            pawn++;

                        }
                        else if (board[y, x] != null && board[y, x].player == Player.PlayerTwo)
                        {
                            pawn = 8;
                            break;
                        }
                        if (pawn > 1)
                        {
                           // MessageBox.Show("Chcesz przeskoczyć przez więcej niż jednego piona");
                        }
                    }
                    if (pawn == 0)
                    {
                        TurnMove(startX, startY, endX, endY);
                    }
                    else if (pawn == 1)
                    {
                        TurnKill(szachownica, endX, endY, centerX, centerY);
                    }
                    else if (pawn == 8)
                    {
                      //  MessageBox.Show("Probujesz zbic swojego piona!");

                    }

                }
            }


            Board_manager board_mamnger_tmp = new Board_manager();
            int[] moveFromTmp = new int[] { startX, startY };
            int[] moveToTmp = new int[] { endX, endY };
            string[] moveFromTmp2 = board_mamnger_tmp.ReturnLetterPosition(moveFromTmp);
            string[] moveToTmp2 = board_mamnger_tmp.ReturnLetterPosition(moveToTmp);
            SetLog("Ruch z: " + string.Join("\n", moveFromTmp2) + " do: " + string.Join("\n", moveToTmp2));
        }



        void TurnKill(Scheme szachownica, int endX, int endY, int centerX, int centerY)
        {
            transformed = TransformationPawn(szachownica, endY);
            board.move(szachownica, endX, endY);
            board.Remove(centerX, centerY);
            removed = true;
            removedX = centerX;
            removedY = centerY;
            kill = false;
            valid = true;

            if (!PawnCanKill(szachownica, endX, endY))
            {
                Board_manager board_mamnger_tmp = new Board_manager();

                int[] removedTmp = new int[] { endX, endY };
                string[] removedTmp2 = board_mamnger_tmp.ReturnLetterPosition(removedTmp);
                SetLog("Usuniętio piona z: " + string.Join("\n", removedTmp));

                ChangeTurn();
            }

            CheckWin();
        }
        void TurnKillK(Scheme szachownica, int endX, int endY, BoardPoint point)
        {
            transformed = TransformationPawn(szachownica, endY);
            board.move(szachownica, endX, endY);
            board.Remove(point.X, point.Y);
            removed = true;
            removedX = point.X;
            removedY = point.Y;
            kill = false;
            valid = true;

            if (!PawnCanKill(szachownica, endX, endY))
                ChangeTurn();

            CheckWin();
        }





        //zmiana piona w królową
        public bool TransformationPawn(Scheme draftboard, int endY)
        {
            bool transformed;
            if (draftboard.pawn != PawnType.PawnQueen &&
                (draftboard.Player == Player.PlayerOne && endY == 7) ||
                (draftboard.Player == Player.PlayerTwo && endY == 0))
            {
                draftboard.pawn = PawnType.PawnQueen;
                transformed = true;
                SetLog("Pion zamieniony na damkę");
            }
            else transformed = false;


            return transformed;

        }
    }
}
