﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game_logic
{
    public class Int_board
    {
        public Scheme[,] board = new Scheme[8, 8];
       public  int[,] int_board = new int[8, 8];

        public int[,] copy_board(Scheme[,] board)
        {
            for (int j = 0; j < 8; j++)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (board[i, j].player == Player.PlayerOne && board[i, j].pawn == PawnType.Pawn)
                    {
                        int_board[i, j] = 1;
                    }
                    else if (board[i,j].player == Player.PlayerOne && board[i,j].pawn == PawnType.PawnQueen)
                    {
                        int_board[i, j] = 3;
                    }
                    else if (board[i, j].player == Player.PlayerTwo && board[i, j].pawn == PawnType.Pawn)
                    {
                        int_board[i, j] = 2;
                    }
                    else if (board[i, j].player == Player.PlayerTwo && board[i, j].pawn == PawnType.PawnQueen)
                    {
                        int_board[i, j] = 4;
                    }
                }
            }
            return int_board;
        }
    }
}
