﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game_logic
{
    public enum PawnType : byte
    {
        None = 0x01,
        Pawn = 0x02,
        PawnQueen = 0x03
    }
}
