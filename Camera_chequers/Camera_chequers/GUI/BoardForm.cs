﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;


namespace GUI
{
    public partial class BoardForm : Form
    {
        class MyPoint
        {
            public int from;
            public int to;
            public MyPoint(int X, int Y)
            {
                this.from = X;
                this.to = Y;
            }
            ~MyPoint() { }
        }
        bool calibrated;
        Capture _Capture;
        public bool captureInProgress;
        public bool saveToFile;
        Game_logic.Rule ruler;
        Image<Bgr, Byte> img; // image captured
        Image<Gray, Byte> processed;
        Image<Gray, Byte> processedGreen;
        Image<Gray, Byte> processedRed;
        Image<Gray, Byte> Gray_Frame2;
        Image<Bgr, Byte> imageimput;
        int[,] chessboard = new int[8, 8];
        int[,] positions = new int[8, 8];
        int[] margin = new int[2];
        List<MyPoint> x = new List<MyPoint>();
        List<MyPoint> y = new List<MyPoint>();
        List<MyPoint> sortedx = new List<MyPoint>();
        List<MyPoint> sortedy = new List<MyPoint>();
        const int width = 7;//9 //width of chessboard no. squares in width - 1
        const int height = 7;//6 // heght of chess board no. squares in heigth - 1
        Size patternSize = new Size(width, height); //size of chess board to be detected
        PointF[] corners; //corners found from chessboard
        Bgr[] line_colour_array = new Bgr[width * height]; // just for displaying coloured lines of detected chessboard
        static Image<Gray, Byte>[] Frame_array_buffer = new Image<Gray, byte>[100]; //number of images to calibrate camera over
        private Button button3;
        private Button button2;
        private PictureBox pictureBox3;
        private Button makemove;
        private PictureBox pictureBox2;
        private PictureBox camerabox;
        delegate void SetLogCall(string tekst);
        public BoardForm()
        {
            InitializeComponent();
             ruler= new Game_logic.Rule(this);
            chequers_board = new Board_manager();
            DrawLetters();
            DrawBoard();
            calibrated = false;
            Random R = new Random();
            for (int i = 0; i < line_colour_array.Length; i++)
            {
                line_colour_array[i] = new Bgr(R.Next(0, 255), R.Next(0, 255), R.Next(0, 255));
            }
            try
            {
                //Image<Bgr, Byte> imageimput2 = new Image<Bgr, Byte>(@"C:\testy\chessfinal.jpg");
               // camerabox.Image = imageimput2.ToBitmap();
                _Capture = new Capture();
                _Capture.ImageGrabbed += new Emgu.CV.Capture.GrabEventHandler(ProcessFrame);
                _Capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }

        }

      
        Board_manager chequers_board;
        private void ProcessFrame(object sender, EventArgs arg)
        {
            IntrinsicCameraParameters IC = new IntrinsicCameraParameters();
            ExtrinsicCameraParameters[] EX_Param;

            img = _Capture.RetrieveBgrFrame();
            processed = img.Convert<Gray, Byte>();
            corners = CameraCalibration.FindChessboardCorners(processed, patternSize, Emgu.CV.CvEnum.CALIB_CB_TYPE.ADAPTIVE_THRESH);
            if (corners != null) //chess board found
            {

                //make mesurments more accurate by using FindCornerSubPixel
                processed.FindCornerSubPix(new PointF[1][] { corners }, new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));

                //if go button has been pressed start aquiring frames else we will just display the points

                //dram the results

                //img.Draw(new CircleF(corners[0], 3), new Bgr(Color.Yellow), 1);
                for (int i = 1; i < corners.Length; i++)
                {
                    //img.Draw(new LineSegment2DF(corners[i - 1], corners[i]), line_colour_array[i], 2);
                    //img.Draw(new CircleF(corners[i], 3), new Bgr(Color.Yellow), 1);
                }
                //kalibracja();
                //calibrate the delay bassed on size of buffer
                //if buffer small you want a big delay if big small delay
                Thread.Sleep(100);//allow the user to move the board to a different position
            }
            camerabox.Image = img.ToBitmap();
            Image<Bgr, Byte> ImageFrame = img;
            if (saveToFile)
            {
                ImageFrame.Save(@"C:\testy\MyPic2.jpg");
                //pictureBox2.Image = ImageFrame.ToBitmap();
                saveToFile = !saveToFile;
            }

            corners = null;
        }
        public int CheckReds(Image<Bgr, Byte> imageimput)
        {
            int countReds = 0;
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal5.jpg");
            Gray_Frame2 = img.InRange(new Bgr(4, 0, 115), new Bgr(98, 93, 255));
            processedRed = Gray_Frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            CircleF[] circles2 = processedRed.HoughCircles(new Gray(85), new Gray(40), 2, 30, 10, 30)[0];
            int s = circles2.Count();
            //tutaj następuje określenie i wpisanie do tablicy miejsc występowania pionków zielonych
            foreach (CircleF cricle in circles2)
            {
                countReds++;
                if (sortedx.Count != 0 && sortedy.Count != 0)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if ((int)cricle.Center.X > sortedx[j * 4].from && (int)cricle.Center.X < sortedx[j * 4].to
                                && (int)cricle.Center.Y > sortedy[i * 4].from && (int)cricle.Center.Y < sortedy[i * 4].to)
                            {
                                if ((i + j) % 2 != 0)
                                {
                                    positions[i, j] = 2;
                                }
                                else positions[i, j] = 0;
                            }
                        }
                    }
                }
                MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                img.Draw(countReds.ToString(), ref f2, new Point((int)cricle.Center.X - 6, (int)cricle.Center.Y), new Bgr(0, 255, 246));
                img.Draw(cricle, new Bgr(Color.Green), 3); //rysowanie na obrazku pionków 
            }
            return countReds;
        }
        public int CheckGreens(Image<Bgr, Byte> imageimput)
        {
            int countGreens = 0;
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal5.jpg");
            Image<Hsv, Byte> hsv_frame1 = img.Convert<Hsv, Byte>();
            Gray_Frame2 = hsv_frame1.InRange(new Hsv(55, 80, 50), new Hsv(100, 255, 255));
            //Gray_Frame2 = img.InImageRange(new Bgr(0, 140, 0), new Bgr(160, 254, 110));
            processedGreen = Gray_Frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            CircleF[] circles2 = processedGreen.HoughCircles(new Gray(85), new Gray(40), 2, 30, 10, 30)[0];
            int s = circles2.Count();
            //tutaj następuje określenie i wpisanie do tablicy miejsc występowania pionków zielonych
            foreach (CircleF cricle in circles2)
            {
                countGreens++;
                if (sortedx.Count != 0 && sortedy.Count != 0)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if ((int)cricle.Center.X > sortedx[j * 4].from && (int)cricle.Center.X < sortedx[j * 4].to
                                && (int)cricle.Center.Y > sortedy[i * 4].from && (int)cricle.Center.Y < sortedy[i * 4].to)
                            {
                                if ((i + j) % 2 != 0)
                                {
                                    positions[i, j] = 1;
                                }
                                else positions[i, j] = 0;
                            }
                        }
                    }
                }
                MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                img.Draw(countGreens.ToString(), ref f2, new Point((int)cricle.Center.X - 6, (int)cricle.Center.Y), new Bgr(0, 255, 246));
                img.Draw(cricle, new Bgr(Color.Red), 3); //rysowanie na obrazku pionków 
            }
            //camerabox.Image = imageimput.ToBitmap();
            return countGreens;
        }
        //FIsza to dla ciebie zebys mogl obsluzyc wykryty ruch
        public int[] movedetection(int[,] chessboard, int[,] newchessboard)
        {
            int oldX = -1;
            int oldY = -1;
            int newX = -1;
            int newY = -1;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {

                    if (chessboard[i, j] == 0 && (newchessboard[i, j] == 1 || newchessboard[i, j] == 2))
                    {
                        newX = j;
                        newY = i;
                    }
                    else if (newchessboard[i, j] == 0 && (chessboard[i, j] == 1 || chessboard[i, j] == 2))
                    {
                        oldX = j;
                        oldY = i;
                    }

                }
            }
            return new int[] { oldX, oldY, newX, newY };
        }
        private void kalibracja()
        {
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal.jpg");
            Image<Gray, Byte> Gray_frame2 = img.InRange(new Bgr(135, 132, 139), new Bgr(255, 255, 255));
            Image<Gray, Byte> Gray_frame = Gray_frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            List<MCvBox2D> boxList = new List<MCvBox2D>();
            using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
                for (Contour<Point> contours = Gray_frame.FindContours(); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                    if (contours.Area > 60 && contours.Area < 10000) //only consider contours with area greaterthan 25010
                    {
                        if (currentContour.Total == 4) //The contour has 4 vertices.
                        {
                            #region determine if all the angles in the contour are within the range of [80, 100] degree
                            bool isRectangle = true;
                            Point[] pts = currentContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                            for (int i = 0; i < edges.Length; i++)
                            {
                                double angle = Math.Abs(
                                   edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                                if (angle < 85 || angle > 95)
                                {
                                    isRectangle = false;
                                    break;
                                }
                            }
                            #endregion

                            if (isRectangle) boxList.Add(currentContour.GetMinAreaRect());
                        }
                    }
                }
            int liczba = 0;
            int tmp = 0;
            int c = boxList.Count;
            if (c != 32) Console.WriteLine("Nie ma, skalibruj jeszcze raz");
            else
            {
                foreach (MCvBox2D box in boxList)
                {
                    liczba++;
                    MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                    img.Draw(liczba.ToString(), ref f2, new Point((int)box.center.X - 6, (int)box.center.Y), new Bgr(Color.Red));
                    img.Draw(box, new Bgr(Color.Red), 1);
                    tmp = (int)box.size.Width / 2;
                    MyPoint newPointX = new MyPoint((int)box.center.X - tmp, (int)box.center.X + tmp);
                    x.Add(newPointX);
                    tmp = (int)box.size.Height / 2;
                    MyPoint newPointY = new MyPoint((int)box.center.Y - tmp, (int)box.center.Y + tmp);
                    y.Add(newPointY);

                }
                
                sortedx = x.OrderBy(o => o.from).ToList();
                sortedy = y.OrderBy(o => o.from).ToList();
                x.Clear();
                y.Clear();

            }
        }
        public void DrawBoard()
        {

            for (int i = 0; i < this.chequers_board.row_size; ++i)
            {
                for (int j = 0; j < this.chequers_board.column_size; ++j)
                {
                    this.Controls.Add(this.chequers_board.boardPicture[i, j]);
                }

            }
            SetLog("Przygotowano szachownicę");
        }
        public void CopyNumericalBoards(Game_logic.Rule ruler_tmp)
        {
            for (int j = 0; j < 8; j++)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (ruler_tmp.board[i, j].player == Game_logic.Player.PlayerOne && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.Pawn)
                    {
                        // biały pionek 
                        this.chequers_board.boardNumerical[i, j] = 1;
                    }
                    else if (ruler_tmp.board[i, j].player == Game_logic.Player.PlayerOne && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.PawnQueen)
                    {
                        //  biała królowa
                        this.chequers_board.boardNumerical[i, j] = 3;
                    }
                    else if (ruler_tmp.board[i, j].player == Game_logic.Player.PlayerTwo && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.Pawn)
                    {
                        //  czarny pionek 
                        this.chequers_board.boardNumerical[i, j] = 2;
                    }
                    else if (ruler_tmp.board[i, j].player == Game_logic.Player.PlayerTwo && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.PawnQueen)
                    {
                        //czarna królowa
                       this. chequers_board.boardNumerical[i, j] = 4;
                    }
                    else if (ruler_tmp.board[i, j] == null)
                        this.chequers_board.boardNumerical[i, j] = 0;///pole puste
                }
            }
        }
        public void DrawBoard(Game_logic.Rule ruler_tmp)
        {
            for (int j = 0; j < 8; j++)
            {
                for (int i = 0; i < 8; i++)
                {
                    if (ruler_tmp.board[i,j]!= null &&ruler_tmp.board[i, j].player == Game_logic.Player.PlayerOne && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.Pawn)
                    {
                        // biały pionek 
                        this.chequers_board.boardNumerical[i, j] = 1;
                    }
                    else if (ruler_tmp.board[i, j] != null&&ruler_tmp.board[i, j].player == Game_logic.Player.PlayerOne && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.PawnQueen)
                    {
                        //  biała królowa
                        this.chequers_board.boardNumerical[i, j] = 3;
                    }
                    else if (ruler_tmp.board[i, j] != null&&ruler_tmp.board[i, j].player == Game_logic.Player.PlayerTwo && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.Pawn)
                    {
                        //  czarny pionek 
                        this.chequers_board.boardNumerical[i, j] = 2;
                    }
                    else if (ruler_tmp.board[i, j] != null && ruler_tmp.board[i, j].player == Game_logic.Player.PlayerTwo && ruler_tmp.board[i, j].pawn == Game_logic.PawnType.PawnQueen)
                    {
                        //czarna królowa
                        this.chequers_board.boardNumerical[i, j] = 4;
                    }
                    else if (ruler_tmp.board[i, j] == null)
                        this.chequers_board.boardNumerical[i, j] = 0;///pole puste
                }
            }

            this.chequers_board.PlaceFigures(this.chequers_board.boardNumerical);
            for (int i = 0; i < this.chequers_board.row_size; ++i)
            {
                for (int j = 0; j < this.chequers_board.column_size; ++j)
                {
                    this.Controls.Add(this.chequers_board.boardPicture[i, j]);
                }

            }
        }
        public void DrawLetters()
        {
            //for (int i = 0; i < this.chequers_board.row_size; ++i)
            //{
            //    this.Controls.Add(this.chequers_board.rowLabels[i]);
            //}
            this.Controls.AddRange(this.chequers_board.rowLabels);

            //for (int j = 0; j < this.chequers_board.column_size; ++j)
            //{
            //    this.Controls.Add(this.chequers_board.columnLabels[j]);
            //}
            this.Controls.AddRange(this.chequers_board.columnLabels);
            //  WriteToLog("Szachownica gotowa do gry");

        }
        private void SetLog(string txt)
        {
            if (LogTextBox.InvokeRequired)
            {
                SetLogCall f = new SetLogCall(SetLog);
                this.Invoke(f, new object[] { txt });
            }
            else
            {
                LogTextBox.AppendText("#" + txt + "\n");
            }
        }
        //public void WriteToLog(string txt)
        //{
        //    LogTextBox.AppendText("#" + txt + "\n");
        //}

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {


                string move = "";/// textBox2.Text;
                string[] submove = move.Split(',');
                string moveHelper = submove.ElementAt(0);
                string[] moveFromTmp = { moveHelper.ElementAt(0) + "", moveHelper.ElementAt(1) + "" };
                moveHelper = submove.ElementAt(1);
                string[] moveToTmp = { moveHelper.ElementAt(0) + "", moveHelper.ElementAt(1) + "" };
                int[] moveFrom = chequers_board.ReturnTablePosition(moveFromTmp);
                int[] moveTo = chequers_board.ReturnTablePosition(moveToTmp);
                //int size = 4;
                //string move = textBox2.Text;
                //string[] submove = move.Split(',');
                //int[] moves = new int[size];
                //for (int i = 0; i < size; i++)
                //    moves[i] = int.Parse(submove[i]);
                chequers_board.MoveFigure(moveFrom[0], moveFrom[1], moveTo[0], moveTo[1]);

                SetLog("Ruch z: " + string.Join("\n", moveFromTmp) + " do: " + string.Join("\n", moveToTmp));
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }
        ///Ruch pionka
        //try
        //    {


        //        string move = textBox2.Text;
        //string[] submove = move.Split(',');
        //string moveHelper = submove.ElementAt(0);
        //string[] moveFromTmp = { moveHelper.ElementAt(0) + "", moveHelper.ElementAt(1) + "" };
        //moveHelper = submove.ElementAt(1);
        //        string[] moveToTmp = { moveHelper.ElementAt(0) + "", moveHelper.ElementAt(1) + "" };
        //int[] moveFrom = chequers_board.ReturnTablePosition(moveFromTmp);
        //int[] moveTo = chequers_board.ReturnTablePosition(moveToTmp);

        ////wersja w formacie pozycji tabeli zamiast LiteraLizba
        ////int size = 4;
        ////string move = textBox2.Text;
        ////string[] submove = move.Split(',');
        ////int[] moves = new int[size];
        ////for (int i = 0; i < size; i++)
        ////    moves[i] = int.Parse(submove[i]); ///koniec wersji


        //chequers_board.MoveFigure(moveFrom[0], moveFrom[1], moveTo[0], moveTo[1]);

        //        WriteToLog("Ruch z: " + string.Join("\n", moveFromTmp)  + " do: " + string.Join("\n", moveToTmp));
        //    }
        //    catch (Exception exception)
        //    {
        //        MessageBox.Show(exception.Message);
        //    }

        private void button5_Click(object sender, EventArgs e)
        {

            try
            {
                string takedown = "";/// textBox2.Text;
                string[] subtakedown = { takedown.ElementAt(0) + "", takedown.ElementAt(1) + "" };
                int[] remove = chequers_board.ReturnTablePosition(subtakedown);

                //int size = 2;
                //string takedown = textBox2.Text;
                //string[] subtakedown = takedown.Split(',');
                //int[] remove = new int[size];
                //for (int i = 0; i < size; i++)
                //    remove[i] = int.Parse(subtakedown[i]);
                chequers_board.RemoveFigure(remove[0], remove[1]);
                SetLog("Bicie na: " + takedown);

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

        }
        ///bicie pionka
        //     try {
        //            string takedown = textBox2.Text;
        //    string[] subtakedown = { takedown.ElementAt(0) + "", takedown.ElementAt(1) + "" };
        //    int[] remove = chequers_board.ReturnTablePosition(subtakedown);

        ////wersja w formacie pozycji tabeli zamiast LiteraLizba
        //    //int size = 2;
        //    //string takedown = textBox2.Text;
        //    //string[] subtakedown = takedown.Split(',');
        //    //int[] remove = new int[size];
        //    //for (int i = 0; i < size; i++)
        //    //    remove[i] = int.Parse(subtakedown[i]); //koniec wersji

        //    chequers_board.RemoveFigure(remove[0], remove[1]);
        //            WriteToLog("Bicie na: " + takedown);

        //}
        //        catch (Exception exception)
        //        {
        //            MessageBox.Show(exception.Message);
        //        }

        //    }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {


                string queen = "";/// textBox2.Text;
                string[] subqueen = { queen.ElementAt(0) + "", queen.ElementAt(1) + "" };
                int[] queens = chequers_board.ReturnTablePosition(subqueen);


                //int size = 2;
                //string queen = textBox2.Text;
                //string[] subqueen = queen.Split(',');
                //int[] queens = new int[size];
                //for (int i = 0; i < size; i++)
                //    queens[i] = int.Parse(subqueen[i]);
                chequers_board.TransformToQueen(queens[0], queens[1]);
                SetLog("Zamieniono na damkę pion na polu: " + queen);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
        //zmiana na królową
        //    try
        //        {


        //            string queen = textBox2.Text;
        //    string[] subqueen = { queen.ElementAt(0) + "", queen.ElementAt(1) + "" };
        //    int[] queens = chequers_board.ReturnTablePosition(subqueen);

        ////wersja w formacie pozycji tabeli zamiast LiteraLizba
        //    //int size = 2;
        //    //string queen = textBox2.Text;
        //    //string[] subqueen = queen.Split(',');
        //    //int[] queens = new int[size];
        //    //for (int i = 0; i < size; i++)
        //    //    queens[i] = int.Parse(subqueen[i]); ///kooniec wersji

        //    chequers_board.TransformToQueen(queens[0], queens[1]);
        //            WriteToLog("Zamieniono na damkę pion na polu: " + queen);
        //}
        //        catch (Exception exception)
        //        {
        //            MessageBox.Show(exception.Message);
        //        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            chequers_board.PlaceFigures(chequers_board.boardPicture, chequers_board.boardNumerical);

        }

        private void rozpocznijGręToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool kalibracja = true;//usun to
            if (kalibracja == true)
            {

                chequers_board.PlaceFigures(chequers_board.boardPicture, chequers_board.boardNumerical);
            }
            else
            {
                MessageBox.Show("Problem z kalibracją, sprawdź czy wszystkie pionki są na swoich miejscach i ponów akcję", "Błąd kalibracji", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void powrótDoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (!calibrated)
            {
                imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal.jpg");
                MessageBox.Show("Program rozpocznie kalibrację, nie zakończy się do momentu gdy wszystkie pionki bedą na planszy", "Kalibracja", MessageBoxButtons.OK, MessageBoxIcon.Information);
                while (sortedx.Count != 32 && sortedy.Count != 32)
                {
                    kalibracja();
                }
                calibrated = true;

                while (CheckReds(imageimput) != 12 && CheckGreens(imageimput) != 12)
                {
                    CheckReds(imageimput);
                    CheckGreens(imageimput);

                }
                ruler.Start();
                DrawBoard(ruler); //draw board based on ruler.board
              //  chequers_board.PlaceFigures(chequers_board.boardPicture, chequers_board.boardNumerical); //draw board from Board_manager
                SetLog("Kalibracja ukonczona pomyslnie");
                SetLog("Przygotowano szachownicę");
                makemove.Text = "Ruch";
                toolTip1.SetToolTip(makemove, "Kliknij, aby zmienić turę.");

            }
            else
            {   
                int x = 1;
                if (x == 1) imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal2.jpg");
                if (x == 2) imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal3.jpg");
                //if (i == 1) imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal2.jpg");
                //if (i == 1) imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal2.jpg"); 
                   
                int[] moves = new int[]{-1,-1,-1,-1};
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {
                        positions[i, j] = 0;
                    }
                }
                while (moves[0] == -1 || moves[1] == -1 || moves[2]==-1||moves[3]==-1)
                {
                    CheckGreens(imageimput);
                    CheckReds(imageimput);
                    moves = movedetection(chequers_board.boardNumerical, positions);
                    Console.Write("Nowa tablica : \n");
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {

                            Console.Write(positions[i, j] + " ");

                        }
                        Console.Write("\n");
                    }
                    Console.Write("Stara tablica : \n");
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {

                            Console.Write(chequers_board.boardNumerical[i, j] + " ");//Tutaj  wyświetla poprzedni stan tabeli, o to chodzilo?, bo dopiero w DrawBoard(Rule) jest odwzorowanie

                        }
                        Console.Write("\n");
                    }

                }//tutaj zamiast ruler board trzeba dac tablice int[8,8] 
                //tam mieliscie sie dogadac jak zmieniacie na inty tablice jedrzeja 
                //u mnie 2 to zielony 1 to czerwony - zmienilem na ze 1 to zielone a 2 czerwony
                x++;
                ruler.Turn(moves[0], moves[1], moves[2], moves[3]);
                bool movevalid = ruler.valid; // zwraca true jezeli ruch jest valid
                bool remove = ruler.removed; // zwraca true jezeli wystepuje bicie
                bool transformed = ruler.transformed; // zwraca true jezeli wystepuje zamiana pionka na damke
                DrawBoard(ruler);
                //if (movevalid == true)
                //{
                //    chequers_board.MoveFigure(moves[1], moves[0], moves[3], moves[2]);
                //}
                //if (remove == true)
                //{
                //    chequers_board.RemoveFigure(moves[1], moves[0]);
                //}
                //if (transformed == true)
                //{
                //    chequers_board.TransformToQueen(moves[1], moves[0]);
                //}

            }
           


        }

        private void plikToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ReturnToMenuButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void powrótDoMenuToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
        //how to use converter from table position to letterrNumber position
        //int [] tmp = { int.Parse(queen.ElementAt(0)+""), int.Parse(queen.ElementAt(1)+"") };
        //string tmp2 = chequers_board.ReturnLetterPosition(tmp).ElementAt(0)+""+chequers_board.ReturnLetterPosition(tmp).ElementAt(1);
    }

