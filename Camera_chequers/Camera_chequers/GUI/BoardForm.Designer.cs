﻿namespace GUI
{
    partial class BoardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BoardForm));
            this.LogTextBox = new System.Windows.Forms.TextBox();
            this.camerabox = new System.Windows.Forms.PictureBox();
            this.makemove = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.powrótDoMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.camerabox)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogTextBox
            // 
            this.LogTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.LogTextBox.Location = new System.Drawing.Point(445, 30);
            this.LogTextBox.Multiline = true;
            this.LogTextBox.Name = "LogTextBox";
            this.LogTextBox.ReadOnly = true;
            this.LogTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.LogTextBox.Size = new System.Drawing.Size(256, 178);
            this.LogTextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.LogTextBox, "Tu wyświetlana jest historia rozgrywki.");
            // 
            // camerabox
            // 
            this.camerabox.Location = new System.Drawing.Point(445, 214);
            this.camerabox.Name = "camerabox";
            this.camerabox.Size = new System.Drawing.Size(256, 184);
            this.camerabox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.camerabox.TabIndex = 148;
            this.camerabox.TabStop = false;
            this.toolTip1.SetToolTip(this.camerabox, "Tu wyświetlany jest obraz z kamery.");
            // 
            // makemove
            // 
            this.makemove.Location = new System.Drawing.Point(445, 405);
            this.makemove.Name = "makemove";
            this.makemove.Size = new System.Drawing.Size(257, 49);
            this.makemove.TabIndex = 149;
            this.makemove.Text = "Kalibracja";
            this.toolTip1.SetToolTip(this.makemove, "Kliknij, aby rozpocząć grę.");
            this.makemove.UseVisualStyleBackColor = true;
            this.makemove.Click += new System.EventHandler(this.button1_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(714, 24);
            this.menuStrip1.TabIndex = 151;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.powrótDoMenuToolStripMenuItem,
            this.zamknijToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // powrótDoMenuToolStripMenuItem
            // 
            this.powrótDoMenuToolStripMenuItem.Name = "powrótDoMenuToolStripMenuItem";
            this.powrótDoMenuToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.powrótDoMenuToolStripMenuItem.Text = "Powrót do Menu";
            this.powrótDoMenuToolStripMenuItem.Click += new System.EventHandler(this.powrótDoMenuToolStripMenuItem_Click_1);
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // BoardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Camera_chequers.Properties.Resources.background_board;
            this.ClientSize = new System.Drawing.Size(714, 466);
            this.ControlBox = false;
            this.Controls.Add(this.makemove);
            this.Controls.Add(this.camerabox);
            this.Controls.Add(this.LogTextBox);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BoardForm";
            this.Text = "Warcaby";
            ((System.ComponentModel.ISupportInitialize)(this.camerabox)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        //private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.TextBox LogTextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem powrótDoMenuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
    }
}

