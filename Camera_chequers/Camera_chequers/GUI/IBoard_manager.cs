﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    interface IBoard_manager
    {
        void FillBoards(PictureBox[,]arrayPic, int[,]arrayNum );
        void InsertLabels(Label[] row, Label[] column);
        void PlaceFigures(PictureBox[,] arrayPic, int[,] arrayNum);
        void PlaceFiguresBlack(PictureBox[,] arrayPic, int[,] arrayNum);
        void PlaceFiguresWhite(PictureBox[,] arrayPic, int[,] arrayNum);
        void MoveFigure( int rowBegin, int columnBegin, int rowEnd, int columnEnd);
        void MoveFigurePic(PictureBox[,] arrayPic, int[,] arrayNum, int rowBegin, int columnBegin, int rowEnd, int columnEnd);
        void MoveFigureNum( int[,] arrayNum, int rowBegin, int columnBegin, int rowEnd, int columnEnd);
        void RemoveFigure(int row, int column);
        void RemoveFigurePic(PictureBox[,] arrayPic, int row, int column);
        void RemoveFigureNum(int[,] arrayNum, int row, int column);
        void RemovedWhite();//method to pass info about takedown
        void RemovedBlack();//method to pass info about takedown
        void TransformToQueen(int row, int column);
        void TransformToQueenWhite(PictureBox[,] arrayPic, int[,] arrayNum, int row, int column);
        void TransformToQueenBlack(PictureBox[,] arrayPic, int[,] arrayNum, int row, int column);
        int[] ReturnTablePosition(string[] positionLetters); //return physical position based on letterNumber location A8 --> 0,0
        string[] ReturnLetterPosition(int[] positionNumbers);//return board position based on table physicial location 0,0 --> A8
        void FillBoardLetters();
        void FillBoardNumbers();
        void PlaceFigures(int[,] arrayNum);


    }
}
