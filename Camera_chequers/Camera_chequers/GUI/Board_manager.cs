﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    class Board_manager : IBoard_manager
    {
        public  int column_size { get; set; } //= 8;
        public  int row_size { get; set; }// = 8;
        public PictureBox[,] boardPicture { get; set; }
        public Label[] rowLabels { get; set; }
        public Label[] columnLabels { get; set; }
        public int[,] boardNumerical { get; set; }
        private static  string[] boardLetters { get; set; } //= { "A", "B", "C", "D", "E", "F", "G", "H" };
        private static int[] boardNumbers { get; set; } //= { 8, 7, 6, 5, 4, 3, 2, 1 };

        public Board_manager()
        {
            column_size = 8;
            row_size = 8;
            boardLetters = new string[column_size];
            boardNumbers = new int[row_size];
            FillBoardLetters();
            FillBoardNumbers();
            boardPicture = new PictureBox[row_size, column_size];
            boardNumerical = new int[row_size, column_size];
            rowLabels = new Label[row_size];
            columnLabels = new Label[column_size];
            FillBoards(boardPicture, boardNumerical);
            InsertLabels(rowLabels, columnLabels);
           
       


        }
        public void FillBoardLetters()
        {
            string [] tmp = new string[] { "A", "B", "C", "D", "E", "F", "G", "H" };
            for (int i = 0; i < column_size; ++i)
            {
                boardLetters[i] = tmp[i];
            }
        }
        public void FillBoardNumbers()
        {
            int[] tmp = new int[] { 8, 7, 6, 5, 4, 3, 2, 1 }; 
            for (int i = 0; i < column_size; ++i)
            {
                boardNumbers[i] = tmp[i];
            }
        }
        public void FillBoards(PictureBox[,] arrayPic, int[,] arrayNum)

        {
            int x = 21;
            int y = 41;//371;
            int stepx = 0;//co 50;
            int stepy = 0;//co 50;

            for (int i = 0; i < row_size; ++i)
            {
                for (int j = 0; j < column_size; ++j)
                {
                    arrayPic[i, j] = new PictureBox();
                    if ((i + j) % 2 == 0)
                    {
                       //  arrayPic[i, j].BackColor = System.Drawing.Color.White;
                        arrayPic[i, j].BackgroundImage = Camera_chequers.Properties.Resources.white_background;
                        arrayPic[i, j].Tag = "white";//tag to check what image is in background
                        arrayNum[i, j] = 0;
                    }
                    else
                    {
                        //arrayPic[i, j].BackColor = System.Drawing.Color.Black;
                        arrayPic[i, j].BackgroundImage = Camera_chequers.Properties.Resources.black_background;
                        arrayPic[i, j].Tag="black";//tag to check what image is in background

                        arrayNum[i, j] = 0;
                    }
                    arrayPic[i, j].BringToFront();
                    arrayPic[i, j].BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                    arrayPic[i, j].Location = new System.Drawing.Point(x + stepx, y + stepy);
                    arrayPic[i, j].Name = "pictureBox"+boardLetters[i]+boardNumbers[j];
                    arrayPic[i, j].Size = new System.Drawing.Size(50, 50);
                    arrayPic[i, j].SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;

                    stepx += 50;
                    //this.Controls.Add(array[i,j]);
                }
                stepx = 0;
                stepy += 50;
            }
        }
        public void InsertLabels(Label [] row, Label[] column)
        {
            int x = 5;
            int y = 60;//371;
            int stepx = 0;//co 50;
            int stepy = 0;//co 50;

            for (int i = 0; i < row_size; ++i)
            {
                row[i] = new Label();
                row[i].Text = "" + boardNumbers[i];
                row[i].AutoSize = true;// = new System.Drawing.Size(10, 15);
                row[i].Location = new System.Drawing.Point(x, y + stepy);
                row[i].Visible = true;
                row[i].BackColor = System.Drawing.Color.Transparent;
                row[i].Font = new System.Drawing.Font(row[i].Font, System.Drawing.FontStyle.Bold);
                stepy += 50;
            }
            y = 27;
            x = 38;
                for (int j = 0; j < column_size; ++j)
                {
                column[j] = new Label();
                column[j].Text =  boardLetters[j];
                column[j].AutoSize = true;// = new System.Drawing.Size(10, 15);
                column[j].Location = new System.Drawing.Point(x+stepx, y );
                column[j].Visible = true;
                column[j].BackColor = System.Drawing.Color.Transparent;
                column[j].Font = new System.Drawing.Font(column[j].Font, System.Drawing.FontStyle.Bold);
                stepx +=50;
                 }
            

        }
        public void PlaceFigures(PictureBox[,] arrayPic, int[,] arrayNum)
        {
            PlaceFiguresBlack(arrayPic, arrayNum);
            PlaceFiguresWhite(arrayPic, arrayNum);

        }
        public void PlaceFigures(int[,] arrayNum)
        {
        
        
            for (int i = 0; i < row_size; i++)
                for (int j = 0; j < column_size; j++)
                {
                    if (arrayNum[i, j] == 1)
                    {
                        boardPicture[i, j].Image = Camera_chequers.Properties.Resources.white_figure;
                        
                    }

                    if (arrayNum[i, j] == 2)
                    {
                        boardPicture[i, j].Image = Camera_chequers.Properties.Resources.black_figure;

                    }
                    if (arrayNum[i, j] == 0)
                    {
                        if (boardPicture[i, j].Tag.Equals("black"))
                            boardPicture[i, j].Image = null;

                    }

                }
        

         }
       
        public void PlaceFiguresWhite(PictureBox[,] arrayPic, int[,] arrayNum)
        {
            for (int i = 0; i < 3; i++) // was from 5 fo 8
                for (int j = 0; j < 8; j++)
                {
                    if (arrayPic[i, j].Tag.Equals("black"))
                    {
                        arrayPic[i, j].Image = Camera_chequers.Properties.Resources.white_figure;
                        arrayNum[i, j] = 1;
                    }

                }
        }
        public void PlaceFiguresBlack(PictureBox[,] arrayPic, int[,] arrayNum)
        {
            for (int i = 5; i < 8; i++)//was from 0 to 3
                for (int j = 0; j < 8; j++)
                {
                    if (arrayPic[i, j].Tag.Equals("black"))
                    {
                        arrayPic[i, j].Image = Camera_chequers.Properties.Resources.black_figure;
                        arrayNum[i, j] = 2;
                    }

                }
        }
        public void MoveFigure(int rowBegin, int columnBegin, int rowEnd, int columnEnd)
        {
            MoveFigureNum(this.boardNumerical, rowBegin, columnBegin, rowEnd, columnEnd);
            MoveFigurePic(this.boardPicture, this.boardNumerical, rowBegin, columnBegin, rowEnd, columnEnd);
        }
        public void MoveFigurePic(PictureBox[,] arrayPic, int[,] arrayNum, int rowBegin, int columnBegin, int rowEnd, int columnEnd)
        {
            arrayPic[rowBegin, columnBegin].Image = null;
            if (arrayNum[rowEnd, columnEnd] == 1)
                arrayPic[rowEnd, columnEnd].Image = Camera_chequers.Properties.Resources.white_figure;
            if (arrayNum[rowEnd, columnEnd] == 2)
                arrayPic[rowEnd, columnEnd].Image = Camera_chequers.Properties.Resources.black_figure;
        }
        public void MoveFigureNum(int[,] arrayNum, int rowBegin, int columnBegin, int rowEnd, int columnEnd)
        {
            int figureType = arrayNum[rowBegin, columnBegin];//1 is white 2 is black 0 is null

            if (figureType == 1)
            {
                arrayNum[rowBegin, columnBegin] = 0;
                arrayNum[rowEnd, columnEnd] = 1;
                return;
            }
            if (figureType == 2)
            {
                arrayNum[rowBegin, columnBegin] = 0;
                arrayNum[rowEnd, columnEnd] = 2;
                return;
            }
            else
            {
                throw new Exception("Nie można wykonać ruchu");
            }
        }
        public void RemoveFigure(int row, int column)
        {
            if (this.boardNumerical[row, column] == 1 || this.boardNumerical[row, column] == 3)
            {
                RemovedWhite();
                RemoveFigureNum(this.boardNumerical, row, column);
                RemoveFigurePic(this.boardPicture, row, column);
                return;

            }

            if (this.boardNumerical[row, column] == 2 || this.boardNumerical[row, column] == 4)
            {
                RemovedBlack();
                RemoveFigureNum(this.boardNumerical, row, column);
                RemoveFigurePic(this.boardPicture, row, column);
                return;

            }
            else
            {
                throw new Exception("Nie można wykonać bicia");
            }

        }
        public void RemoveFigurePic(PictureBox[,] arrayPic, int row, int column)
        {
            arrayPic[row, column].Image = null;
        }
        public void RemoveFigureNum(int[,] arrayNum, int row, int column)
        {
            arrayNum[row, column] = 0;
        }
        public void RemovedWhite()//method to pass info about takedown
        {

        }
        public void RemovedBlack()//method to pass info about takedown
        {

        }
        public void TransformToQueen(int row, int column)
        {
            if (this.boardNumerical[row, column] == 1)
            {
                TransformToQueenWhite(this.boardPicture, this.boardNumerical, row, column);
                return;
            }
            if (this.boardNumerical[row, column] == 2)
            {
                TransformToQueenBlack(this.boardPicture, this.boardNumerical, row, column);
                return;
            }
            else
            {
                throw new Exception("Nie można zamienić na damkę");
            }
        }
        public void TransformToQueenWhite(PictureBox[,] arrayPic, int[,] arrayNum, int row, int column)
        {
            arrayPic[row, column].Image = Camera_chequers.Properties.Resources.white_queen;
            arrayNum[row, column] = 3;//3 mean white queen
        }
        public void TransformToQueenBlack(PictureBox[,] arrayPic, int[,] arrayNum, int row, int column)
        {
            arrayPic[row, column].Image = Camera_chequers.Properties.Resources.black_queen;
            arrayNum[row, column] = 4;//4 mean  black queen
        }
        public int[] ReturnTablePosition(string[] positionLetters)
        {
            int column = Array.FindIndex(boardLetters, columnTmp => columnTmp.Contains(positionLetters[0])); 
            int row = Array.IndexOf(boardNumbers, int.Parse(positionLetters[1]));
            
            int[] position = { row, column };
            return position;
        }

        public string[] ReturnLetterPosition(int[] positionNumbers)
        {
            string column = boardLetters.ElementAt(positionNumbers[0]);
            string row = boardNumbers.ElementAt(positionNumbers[1])+"";
            string[] position = {column,row };
            return position;
        }
    }
}
