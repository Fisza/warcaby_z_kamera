﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI;


namespace Camera_chequers
{
    public partial class MainForm : Form
    {
        BoardForm board;
        string instruction="Gra rozpoczyna się z 24 pionkami na planszy, 12 białych figur(zielonych w programi) i 12 czarnych figur(czerwone w programie).\n Rozpoczyna gracz z białymi pionkami.\n Ruch może odbywać sie na skos, po czarnych polach planszy.\n Jeśli przeciwne pionki znajdują się na przelegających pola to możliwe jest bicie. Jeśli po wykonaniu bicia sytuacja się powtarza możliwe jest kolejne bicie.\n Bicie jest obowiązkowe, za brak zbicia gracz traci pionka i turę.\n Po osiagnieciu przeciwnej krawędzi planszy pionek zmienia się w damkę, która może poruszać się o dowolną liczbę pól przy takich samcych zasadach.\n Powodzenia!";
        string authors = "Filip Fischer ,\nPhi Long Nguyen ,\nJędrzej Górnaś.";
        public MainForm()
        {
            InitializeComponent();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Za chwilę zostaniesz przeniesiony do planszy.", "Informacja", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                
                this.Hide();
                board = new BoardForm();
                board.ShowDialog();
                this.Show();
            }
            else
            {
                //do nothing
            }
        }

        private void HowToPlayButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(instruction, "Zasady", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AuthorsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(authors, "Autorzy", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
