﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Camera_reading
{
    public partial class Form1 : Form
    {
        class MyPoint
        {
            public int from;
            public int to;
            public MyPoint(int X, int Y)
            {
                this.from = X;
                this.to = Y;
            }
            ~MyPoint() { }
        }
        Capture _Capture;
        public bool captureInProgress;
        public bool saveToFile;
        Rule ruler = new Rule();
        Image<Bgr, Byte> img; // image captured
        Image<Gray, Byte> processed;
        Image<Gray, Byte> processedGreen;
        Image<Gray, Byte> processedRed;
        Image<Gray, Byte> Gray_Frame2;
        Image<Bgr, Byte> imageimput;
        int[,] chessboard = new int[8, 8];
        int[,] positions = new int[8, 8];
        int[] margin = new int[2];
        List<MyPoint> x = new List<MyPoint>();
        List<MyPoint> y = new List<MyPoint>();
        List<MyPoint> sortedx = new List<MyPoint>();
        List<MyPoint> sortedy = new List<MyPoint>();
        const int width = 7;//9 //width of chessboard no. squares in width - 1
        const int height = 7;//6 // heght of chess board no. squares in heigth - 1
        Size patternSize = new Size(width, height); //size of chess board to be detected
        PointF[] corners; //corners found from chessboard
        Bgr[] line_colour_array = new Bgr[width * height]; // just for displaying coloured lines of detected chessboard
        static Image<Gray, Byte>[] Frame_array_buffer = new Image<Gray, byte>[100]; //number of images to calibrate camera over
        private Button button3;
        private Button button2;
        private PictureBox pictureBox3;
        private Button button1;
        private PictureBox pictureBox2;
        private PictureBox pictureBox1;
        public Form1()
        {
            InitializeComponent();
            Random R = new Random();
            for (int i = 0; i < line_colour_array.Length; i++)
            {
                line_colour_array[i] = new Bgr(R.Next(0, 255), R.Next(0, 255), R.Next(0, 255));
            }
            try
            {
                _Capture = new Capture();
                _Capture.ImageGrabbed += new Emgu.CV.Capture.GrabEventHandler(ProcessFrame);
                _Capture.Start();
            }
            catch (NullReferenceException excpt)
            {
                MessageBox.Show(excpt.Message);
            }
        }
        private void ProcessFrame(object sender, EventArgs arg)
        {
            IntrinsicCameraParameters IC = new IntrinsicCameraParameters();
            ExtrinsicCameraParameters[] EX_Param;

            img = _Capture.RetrieveBgrFrame();
            Image<Bgr, Byte> imageimput2 = new Image<Bgr, Byte>(@"C:\testy\chess3-1.jpg");
            processed = img.Convert<Gray, Byte>();
            corners = CameraCalibration.FindChessboardCorners(processed, patternSize, Emgu.CV.CvEnum.CALIB_CB_TYPE.ADAPTIVE_THRESH);
            if (corners != null) //chess board found
                {
                    
                    //make mesurments more accurate by using FindCornerSubPixel
                   processed.FindCornerSubPix(new PointF[1][] { corners }, new Size(11, 11), new Size(-1, -1), new MCvTermCriteria(30, 0.1));

                    //if go button has been pressed start aquiring frames else we will just display the points
               
                    //dram the results

                    //img.Draw(new CircleF(corners[0], 3), new Bgr(Color.Yellow), 1);
                    for(int i = 1; i<corners.Length; i++)
                    {
                        //img.Draw(new LineSegment2DF(corners[i - 1], corners[i]), line_colour_array[i], 2);
                        //img.Draw(new CircleF(corners[i], 3), new Bgr(Color.Yellow), 1);
                    }
                    //kalibracja();
                    //calibrate the delay bassed on size of buffer
                    //if buffer small you want a big delay if big small delay
                    Thread.Sleep(100);//allow the user to move the board to a different position
                }
                pictureBox1.Image = img.ToBitmap();
                Image<Bgr, Byte> ImageFrame = img;
                if (saveToFile)
                {
                    ImageFrame.Save(@"C:\testy\MyPic2.jpg");
                    pictureBox2.Image = ImageFrame.ToBitmap();
                    saveToFile = !saveToFile;
                }
                
                corners = null;
            }

        private void Pstryk_Click(object sender, EventArgs e)
        {
            saveToFile = !saveToFile;
        }
        public void CheckReds()
        {
            int countReds = 0;
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal2.jpg");
            Gray_Frame2 = img.InRange(new Bgr(4, 0, 115), new Bgr(98, 93, 255));
            processedRed = Gray_Frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            CircleF[] circles2 = processedRed.HoughCircles(new Gray(85), new Gray(40), 2, 30, 10, 60)[0];
            int s = circles2.Count();
            //tutaj następuje określenie i wpisanie do tablicy miejsc występowania pionków zielonych
            foreach (CircleF cricle in circles2)
            {
                countReds++;
                if (sortedx.Count != 0 && sortedy.Count != 0)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if ((int)cricle.Center.X > sortedx[j * 4].from && (int)cricle.Center.X < sortedx[j * 4].to
                                && (int)cricle.Center.Y > sortedy[i * 4].from && (int)cricle.Center.Y < sortedy[i * 4].to)
                            {
                                positions[i, j] = 1;
                            }
                        }
                    }
                }
                MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                img.Draw(countReds.ToString(), ref f2, new Point((int)cricle.Center.X - 6, (int)cricle.Center.Y), new Bgr(0, 255, 246));
                img.Draw(cricle, new Bgr(Color.Green), 3); //rysowanie na obrazku pionków 
            }
            pictureBox2.Image = Gray_Frame2.ToBitmap();
        }
        public void CheckGreens()
        {
            int countGreens = 0;
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal2.jpg");
            Image<Hsv, Byte> hsv_frame1 = img.Convert<Hsv, Byte>();
            Gray_Frame2 = hsv_frame1.InRange(new Hsv(55, 80, 50), new Hsv(100, 255, 255));
            //Gray_Frame2 = img.InRange(new Bgr(0, 140, 0), new Bgr(160, 254, 110));
            processedGreen = Gray_Frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            CircleF[] circles2 = processedGreen.HoughCircles(new Gray(85), new Gray(40), 2, 30, 10, 30)[0];
            int s = circles2.Count();
            //tutaj następuje określenie i wpisanie do tablicy miejsc występowania pionków zielonych
            foreach (CircleF cricle in circles2)
            {
                countGreens++;
                if (sortedx.Count != 0 && sortedy.Count != 0)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            if ((int)cricle.Center.X > sortedx[j * 4].from && (int)cricle.Center.X < sortedx[j * 4].to
                                && (int)cricle.Center.Y > sortedy[i * 4].from && (int)cricle.Center.Y < sortedy[i * 4].to)
                            {
                                positions[i, j] = 2;
                            }
                        }
                    }
                }
                MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                img.Draw(countGreens.ToString(), ref f2, new Point((int)cricle.Center.X - 6, (int)cricle.Center.Y), new Bgr(0, 255, 246));
                img.Draw(cricle, new Bgr(Color.Red), 3); //rysowanie na obrazku pionków 
            }
            pictureBox2.Image = processedGreen.ToBitmap();
        }
        //FIsza to dla ciebie zebys mogl obsluzyc wykryty ruch
        public int[] movedetection (int [,] chessboard, int[,] newchessboard)
        {
            int oldX = -1;
            int oldY = -1;
            int newX = -1;
            int newY = -1;
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {

                    if(chessboard[i,j] != 0 && (newchessboard[i,j] == 1 || newchessboard[i,j]==2))
                    {
                        newX = j;
                        newY = i;
                    }
                    else if (newchessboard[i, j] != 0 && (chessboard[i, j] == 1 || chessboard[i, j] == 2))
                    {
                        oldX = j;
                        oldY = i;
                    }

                }
            }
            return new int[] { oldX, oldY, newX, newY };
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (positions != null)
            {
                chessboard = positions; 
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 8; j++)
                    {

                        Console.Write(chessboard[i, j] + " ");

                    }
                    Console.Write("\n");
                }
            }
           CheckGreens();
           CheckReds();
           Console.Write("Nowa:\n");
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {

                    Console.Write(positions[i, j] + " ");

                }
                Console.Write("\n");
            }
        }

        private void kalibracja()
        {
            imageimput = new Image<Bgr, Byte>(@"C:\testy\chessfinal.jpg");
            Image<Gray, Byte> Gray_frame2 = img.InRange(new Bgr(135, 132, 139), new Bgr(255, 255, 255));
            Image<Gray, Byte> Gray_frame = Gray_frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            List<MCvBox2D> boxList = new List<MCvBox2D>();
            using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
                for (Contour<Point> contours = Gray_frame.FindContours(); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                    if (contours.Area > 60&&contours.Area<10000) //only consider contours with area greaterthan 25010
                    {
                        if (currentContour.Total == 4) //The contour has 4 vertices.
                        {
                            #region determine if all the angles in the contour are within the range of [80, 100] degree
                            bool isRectangle = true;
                            Point[] pts = currentContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                            for (int i = 0; i < edges.Length; i++)
                            {
                                double angle = Math.Abs(
                                   edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                                if (angle < 85 || angle > 95)
                                {
                                    isRectangle = false;
                                    break;
                                }
                            }
                            #endregion

                            if (isRectangle) boxList.Add(currentContour.GetMinAreaRect());
                        }
                    }
                }
            int liczba = 0;
            int tmp = 0;
            int c = boxList.Count;
            if (c != 32) Console.WriteLine("Nie ma, skalibruj jeszcze raz");
            else
            {
                foreach (MCvBox2D box in boxList)
                {
                    liczba++;
                    MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                    img.Draw(liczba.ToString(), ref f2, new Point((int)box.center.X - 6, (int)box.center.Y), new Bgr(Color.Red));
                    img.Draw(box, new Bgr(Color.Red), 1);
                    tmp = (int)box.size.Width / 2;
                    MyPoint newPointX = new MyPoint((int)box.center.X - tmp, (int)box.center.X + tmp);
                    x.Add(newPointX);
                    tmp = (int)box.size.Height / 2;
                    MyPoint newPointY = new MyPoint((int)box.center.Y - tmp, (int)box.center.Y + tmp);
                    y.Add(newPointY);

                }
                sortedx = x.OrderBy(o => o.from).ToList();
                sortedy = y.OrderBy(o => o.from).ToList();
                x.Clear();
                y.Clear();

                pictureBox3.Image = Gray_frame.ToBitmap();
            }
            pictureBox3.Image = Gray_frame.ToBitmap();
        }

        private void kalibracja2()
        {
            //imageimput = new Image<Bgr, Byte>(@"C:\testy\chess3.jpg");
            Image<Gray, Byte> Gray_frame2 = img.InRange(new Bgr(135, 132, 139), new Bgr(255, 255, 255));
            Image<Gray, Byte> Gray_frame = Gray_frame2.Convert<Gray, Byte>().PyrDown().PyrUp();
            List<MCvBox2D> boxList = new List<MCvBox2D>();
            using (MemStorage storage = new MemStorage()) //allocate storage for contour approximation
                for (Contour<Point> contours = Gray_frame.FindContours(); contours != null; contours = contours.HNext)
                {
                    Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);

                    if (contours.Area > 60 && contours.Area < 200) //only consider contours with area greaterthan 25010
                    {
                        if (currentContour.Total == 4) //The contour has 4 vertices.
                        {
                            #region determine if all the angles in the contour are within the range of [80, 100] degree
                            bool isRectangle = true;
                            Point[] pts = currentContour.ToArray();
                            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

                            for (int i = 0; i < edges.Length; i++)
                            {
                                double angle = Math.Abs(
                                   edges[(i + 1) % edges.Length].GetExteriorAngleDegree(edges[i]));
                                if (angle < 85 || angle > 95)
                                {
                                    isRectangle = false;
                                    break;
                                }
                            }
                            #endregion

                            if (isRectangle) boxList.Add(currentContour.GetMinAreaRect());
                        }
                    }
                }
            int liczba = 0;
            int tmp = 0;
            int c = boxList.Count;
            //if (c != 32) MessageBox.Show("Nie wykryto wszytkich pól, skalibruj jeszcze raz","Błąd");
            foreach (MCvBox2D box in boxList)
            {
                liczba++;
                MCvFont f2 = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_TRIPLEX, 1.0, 1.0);
                img.Draw(liczba.ToString(), ref f2, new Point((int)box.center.X - 6, (int)box.center.Y), new Bgr(Color.Red));
                img.Draw(box, new Bgr(Color.Red), 1);
                tmp = (int)box.size.Width / 2;
                MyPoint newPointX = new MyPoint((int)box.center.X - tmp, (int)box.center.X + tmp);
                x.Add(newPointX);
                tmp = (int)box.size.Height / 2;
                MyPoint newPointY = new MyPoint((int)box.center.Y - tmp, (int)box.center.Y + tmp);
                y.Add(newPointY);

            }
            sortedx = x.OrderBy(o => o.from).ToList();
            sortedy = y.OrderBy(o => o.from).ToList();
            x.Clear();
            y.Clear();
            

            pictureBox3.Image = Gray_frame.ToBitmap();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            //while(sortedx.Count != 32 && sortedy.Count != 32)
            //{
                kalibracja();
            //}
            //MessageBox.Show("Kalibracja ukonczona pomyslnie");
        }

        public bool validate (int x ,int y, int newx, int newy)
        {
           // bool validmove = ruler.board
            return false;
        }

    }
}
