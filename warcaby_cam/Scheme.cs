﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace warcaby_cam
{
    class Scheme
    {
        public Player player;
        public BoardPoint point;
        public PawnType pawn;

        public Scheme(int x, int y, Player player) : this(x, y, PawnType.None, player) { }
        public Scheme(int x, int y, PawnType pawn, Player player) : this(new BoardPoint(y, x), pawn, player) { }
        public Scheme(BoardPoint point, PawnType pawn, Player player)
        {
            this.point = point;
            this.pawn = pawn;
            this.player = player;
        }
        public BoardPoint Point
        {
            get { return point; }
            set { point = value; }
        }

        public int X
        {
            get
            {
                return point.X;
            }
            set
            {
                point.X = value;
            }
        }

        public int Y
        {
            get
            {
                return point.Y;
            }
            set
            {
                point.Y = value;
            }
        }

        public PawnType type
        {
            get
            {
                return pawn;
            }
            set
            {
                pawn = value;
            }
        }

        public Player Player
        {
            get
            {
                return player;
            }
        }
    }
}
