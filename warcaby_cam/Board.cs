﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace warcaby_cam
{
    class Board
    {
        public Scheme[,] board = new Scheme[8, 8];
        public Int_board[,] int_board = new Int_board[8, 8];
        public int PawnPlayerOne;
        public int PawnPlayerTwo;
        public Board()
        {
            create_board();
        }
        public Board(Scheme[,] board, int PawnPlayerOne, int PawnPlayerTwo)
        {
            this.board = board;
            this.PawnPlayerOne = PawnPlayerOne;
            this.PawnPlayerTwo = PawnPlayerTwo;
        }

        public void create_board()
        {
            create_onePlayer(board);
            create_twoPlayer(board);
        }
        public Scheme this[int y, int x]
        {
            get
            {
                return board[y, x];
            }
            set
            {
                board[y, x] = value;
            }
        }
        public void create_onePlayer(Scheme[,] board)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if ((y + x) % 2 != 0)
                    {
                        board[y, x] = new Scheme(x, y, PawnType.Pawn, Player.PlayerOne);
                    }
                }
            }
            PawnPlayerOne = 12;
        }
        public void create_twoPlayer(Scheme[,] board)
        {
            for (int y = 5; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if ((y + x) % 2 != 0)
                    {
                        board[y, x] = new Scheme(x, y, PawnType.Pawn, Player.PlayerTwo);
                    }
                }
            }
            PawnPlayerTwo = 12;
        }
        /*
        internal void move(Draft draught, int endX, int endY)
        {
            throw new NotImplementedException();
        }
        */
        public Scheme[,] szachownica
        {
            get
            {
                return board;
            }
        }

        public void Remove(int x, int y)
        {
            if (board[y, x].player == Player.PlayerOne)
            {
                PawnPlayerOne--;
            }
            else
            {
                PawnPlayerTwo--;
            }
            board[y, x] = null;
        }
        public void Remove(Scheme draught)
        {
            Remove(draught.X, draught.Y);
        }
        public void Remove(BoardPoint point)
        {
            Remove(point.X, point.Y);

        }
        public void move(int oldX, int oldY, int newX, int newY)
        {
            board[newY, newX] = board[oldY, oldX];
            board[newY, newX].X = newX;
            board[newY, newX].Y = newY;
            board[oldY, oldX] = null;
            show_Board();
        }
        public void move(Scheme szachownica, int x, int y)
        {
            if (szachownica == null)
                throw new ArgumentNullException();

            move(szachownica.X, szachownica.Y, x, y);
        }
        public void move(Scheme szachownica, BoardPoint point)
        {
            if (szachownica == null)
                throw new ArgumentNullException();

            move(szachownica.X, szachownica.Y, point.X, point.Y);
        }

        public bool try_move(int oldX, int oldY, int newX, int newY)
        {

            if ((oldX + oldY) % 2 != 0 && (newX + newY) % 2 != 0)
            {
                move(oldX, oldY, newX, newY);
                return true;
            }
            else return false;

        }
        
        public void show()
        {
            Console.Write(board[1, 1]);
        }
        public void show_Board()
        {
            for (int y = 0; y < 8; y++)
            {
                Console.Write('\n');
                for (int x = 0; x < 8; x++)
                {
                    if (board[y, x] != null)
                    {
                        if (board[y, x].player == Player.PlayerOne)
                        {
                            Console.Write(1);
                        }
                        else if (board[y, x].player == Player.PlayerTwo)
                        {
                            Console.Write(2);
                        }
                    }
                    else Console.Write(0);

                }
            }
            Console.Write('\n');

        }
    }
}
